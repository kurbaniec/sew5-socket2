package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * Simple client for the reverse Server that sends an user input.
 * @author Kacper Urbaniec
 * @version 2020-05-13
 */
public class UserClient {
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private Scanner scanner;

    /**
     * Create new client
     * @throws IOException
     */
    public UserClient() throws IOException {
        socket = new Socket("localhost", 8888);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);
        scanner = new Scanner(System.in);
    }

    /**
     * Send request to server and output response to the terminal.
     */
    public void sendRequest() {
        System.out.println("Please enter an input:");
        out.println(scanner.nextLine());
        try {
            System.out.println(in.readLine());
        } catch (IOException e) {
            System.out.println("Something went wrong...");
            e.printStackTrace();
        }
    }

    /**
     * Close client gracefully.
     */
    public void close() {
        try {
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Create and starts a new client.
     * @param args
     */
    public static void main(String[] args) {
        try {
            UserClient client = new UserClient();
            client.sendRequest();
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
