import sys
import socket

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import *

from ui import window


class ClientController(QtWidgets.QMainWindow):

    def __init__(self):
        """
        Initializes PyQt-UI
        """
        super().__init__()
        self.main = window.Ui_MainWindow()
        self.main.setupUi(self)
        self.setMinimumHeight(160)
        self.main.revButton.clicked.connect(self.process)

    def process(self):
        """
        Takes the user input, sends it to the reverse server and outputs the response
        in the right textfield.
        """
        # Get User Input
        text: str = self.main.revInput.toPlainText()
        if text != "":
            # Check for Line-Ending (needed for the server because of `readLine`)
            if not text.endswith("\n"):
                text += "\n"
            response = ""
            try:
                # Create a new socket
                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                    # Connect to the server
                    s.connect(("localhost", 8888))
                    # Send user input to server
                    s.send(text.encode())
                    # Read all incoming data and decode it correctly
                    while True:
                        data = s.recv(1024)
                        if data:
                            response += data.decode('utf-8')
                        else:
                            break
                # Output response
                if response != "":
                    self.main.revOutput.setPlainText(response)
                else:
                    self.main.revOutput.setPlainText("Something went wrong...")
            except Exception as e:
                # All exceptions encountered will be display in the UI
                self.main.revOutput.setPlainText("Something went wrong...\n" + str(e))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    c = ClientController()
    c.show()
    sys.exit(app.exec_())
