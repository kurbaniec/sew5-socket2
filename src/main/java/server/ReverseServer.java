package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Simple server that listens for connections and returns the reversed text of a client input.
 * @author Kacper Urbaniec
 * @version 2020-05-13
 */
public class ReverseServer {
    private ExecutorService executor;
    private ArrayList<ReverseWorker> workers;
    private ServerSocket socket;
    private boolean listening = true;

    /**
     * Creates a new server object.
     * @throws IOException
     */
    public ReverseServer() throws IOException {
        executor = Executors.newCachedThreadPool();
        workers = new ArrayList<>();
        socket = new ServerSocket(8888);
    }

    /**
     * Starts the reverse server.
     */
    public void start() {
        while (listening) {
            try {
                // Wait for connections and create new workers from new ones
                ReverseWorker worker = new ReverseWorker(this, socket.accept());
                // Add worker to list and start them via the executor service
                workers.add(worker);
                executor.submit(worker);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /*+
     * Removes a finished worker (thread) from the workers list.
     */
    public void workerFinished(ReverseWorker worker) {
        workers.remove(worker);
    }

    /**
     * Closes the server gracefully.
     */
    public void shutdown() {
        listening = false;
        executor.shutdown();
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Creates and starts a new reverse sever.
     */
    public static void main(String[] args) {
        try {
            ReverseServer server = new ReverseServer();
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
