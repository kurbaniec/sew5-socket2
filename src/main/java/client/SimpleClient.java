package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Simple client for the reverse Server that sends a predefined text.
 * @author Kacper Urbaniec
 * @version 2020-05-13
 */
public class SimpleClient {
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    /**
     * Create new client
     * @throws IOException
     */
    public SimpleClient() throws IOException {
        socket = new Socket("localhost", 8888);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);
    }

    /**
     * Send request to server and output response to the terminal.
     */
    public void sendRequest() {
        out.println("This is a test!");
        try {
            System.out.println(in.readLine());
        } catch (IOException e) {
            System.out.println("Something went wrong...");
            e.printStackTrace();
        }
    }

    /**
     * Close client gracefully.
     */
    public void close() {
        try {
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Create and starts a new client.
     * @param args
     */
    public static void main(String[] args) {
        try {
            SimpleClient client = new SimpleClient();
            client.sendRequest();
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
