package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Worker (thread) for the reverse Server.
 * @author Kacper Urbaniec
 * @version 2020-05-13
 */
public class ReverseWorker implements Runnable {
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private ReverseServer callback;

    /**
     * Create a new worker.
     * @param callback Reference to the reverse Server
     * @param socket Socket of the new connection
     * @throws IOException
     */
    public ReverseWorker(ReverseServer callback, Socket socket) throws IOException {
        this.callback = callback;
        this.socket = socket;
        this.in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
        this.out = new PrintWriter(this.socket.getOutputStream(), true);
    }

    /**
     * Performs the main business that reverses an received text through the socket connection
     * and sends the reversed text back.
     */
    @Override
    public void run() {
        try {
            // Read input
            String input = in.readLine();
            // Reverse text
            String reversed = processText(input);
            // Send reversed text back
            out.println(reversed);
        } catch (IOException e) {
            out.println("Something went wrong...");
        }
        // Close all resources
        try {
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Mark worker as finished in server
        callback.workerFinished(this);
    }

    /**
     * Reverses a String.
     * @param input String that should be reversed
     * @return The reversed String
     */
    private String reverseString(String input) {
        return new StringBuilder(input).reverse().toString();
    }

    /**
     * Reverses and modifies a String.
     * @param input String received from the client
     * @return The reversed and modified String
     */
    private String processText(String input) {
        // Check if text contains whitespace used for special operations
        int index = input.lastIndexOf(' ');
        // If not, return reversed String
        if (index == -1) {
            return reverseString(input);
        }
        // Check if operations is valid
        String operation = input.substring(index+1);
        // If not, return reversed String
        if (!operation.equals("-u") && !operation.equals("-l")) {
            return reverseString(input);
        }
        // Reverse only the part of the String without operator
        String text = reverseString(input.substring(0, index));
        // Return String with wanted modifying operation
        if (operation.equals("-u")) {
            return text.toUpperCase();
        } else {
            return text.toLowerCase();
        }
    }
}
