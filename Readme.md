# Sockets 2

## **Aufgabe: Implementierung eines einfachen Servers**

Implementiert einen einfachen Server, welcher eine eingehende Nachricht in umgekehrter Form zurückschickt.

Also, schickt der Client z.B. den Text “Hallo!” an den Server, so bekommt er als Antwort “!ollaH” zurück.

Implementiert dazu auch einen entsprechenden Client um den Server zu testen. Es reicht auch, wenn dieser einen vorgefertigten Text verschickt und die Antwort einfach auf der Konsole ausgibt.

Exceptions (Client und Server) sollten auch abgefangen werden und zu sinnvollen Fehlermeldungen führen.

## **Aufgabe: Erweiterung des letzten Beispiels**

* Verwendet Threads. Verbindet sich ein Client, wird ein neuer Thread gestartet, sodass sofort der nächste Client akzeptiert werden kann.
* Die Serverlogik soll über eine eigene Protokollklasse umgesetzt werden.

* Falls ihr bereits die Unterstützung durch Threads Implementiert habt, dann versucht zusätzliche Steuerbefehle einzubauen, die der Nachricht mitgegeben werden können. Z.B. mit -u für UpperCase. "Hallo -u" -> "OLLAH"
* Erweitert den Client um eine einfache GUI.

Achtet auch weiterhin darauf, dass Exceptions (Client und Server) abgefangen werden und zu sinnvollen Fehlermeldungen führen.

## Ausführen


* Server

  `./gradlew server`

* Client

  `./gradlew client`
  
* *Neu* GUI-Client

  `pip install requirements.txt`

  `cd src/main/python`

  `python controller.py`

### Unterstützte Steuerbefehle

* `-u` UpperCase

  z.B. "Hallo -u" => "OLLAH"

* `-l` LowerCase

  z.B. "Toller Satz -l" => "ztas rellot"